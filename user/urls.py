from django.urls import path

from user.views import MyTokenObtainPairView

urlpatterns = [
    path('login', MyTokenObtainPairView.as_view(),),

]
