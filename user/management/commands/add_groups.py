from django.contrib.auth.models import Group
from django.core.management import BaseCommand

from user.permissions import admin_group, owner_group, user_group


class Command(BaseCommand):

    def handle(self, *args, **options):
        tasks = Group.objects.get_or_create(name=admin_group)
        tasks = Group.objects.get_or_create(name=owner_group)
        tasks = Group.objects.get_or_create(name=user_group)
