from rest_framework.permissions import BasePermission

admin_group = 'ADMIN'
owner_group = 'OWNER'
user_group = 'USER'


class AdminPermission(BasePermission):
    message = 'PERMISSION_DENIED'

    def has_permission(self, request, view):
        user = request.user
        if user.groups.filter(name=admin_group).exists():
            return True
        return user.is_superuser


class OwnerPermission(BasePermission):
    message = 'PERMISSION_DENIED'

    def has_permission(self, request, view):
        user = request.user
        if user.groups.filter(name=owner_group).exists():
            return True
        return user.is_superuser

class UserPermission(BasePermission):
    message = 'PERMISSION_DENIED'

    def has_permission(self, request, view):
        user = request.user
        if user.groups.filter(name=user_group).exists():
            return True
        return user.is_superuser