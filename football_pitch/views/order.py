from datetime import timedelta

from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from football_pitch.models import Pitch, PitchOrder
from football_pitch.serializers.book import PitchBookSerializer
from user.permissions import UserPermission, OwnerPermission


class BookPitchView(APIView):
    permission_classes = (UserPermission, )

    @swagger_auto_schema(request_body=PitchBookSerializer)
    def post(self, request):
        user = request.user
        serializer = PitchBookSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        pitch_id = validated_data.get('pitch_id')
        start_activity_time = validated_data.get('start_activity_time')
        activity_hours = validated_data.get('activity_hours')

        pitch = Pitch.objects.get(id=pitch_id)
        finish_activity_time = start_activity_time + timedelta(hours=activity_hours)
        price = activity_hours * pitch.cost_by_hour

        falls = PitchOrder.objects.filter(Q(pitch=pitch) &
            Q(start_activity_time__lt=start_activity_time) &
            Q(finish_activity_time__gt=start_activity_time)
        )
        if falls:
            return Response({
                'data': 'This slot is already booked',
            }, status=status.HTTP_404_NOT_FOUND)
        PitchOrder.objects.create(
            user=user,
            pitch=pitch,
            start_activity_time=start_activity_time,
            finish_activity_time=finish_activity_time,
            activity_hours=activity_hours,
            price=price,
        )
        return Response({
            'data': 'Pitch has been book successfully'
        })


class BookPitchListView(APIView):
    permission_classes = (OwnerPermission|UserPermission, )

    def get(self, request, pk):
        orders = PitchOrder.objects.filter(
            pitch_id=pk,
        ).values('id', 'start_activity_time', 'finish_activity_time', 'price', 'activity_hours')
        return Response(data=orders)


class DeleteBookPitchView(APIView):
    permission_classes = (OwnerPermission, )


    def delete(self, request, pk):
        order = PitchOrder.objects.get(pk=pk)
        order.delete()
        return Response(data={
            'data': 'Booked order deleted!'
        })
