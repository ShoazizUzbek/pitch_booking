from django.db.models import Q
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from math import radians, cos, sin, asin, sqrt

from football_pitch.models import Pitch, PitchMedia
from football_pitch.serializers.pitch import PitchSerializer, PitchModelSerializer
from user.permissions import OwnerPermission, UserPermission


class PitchCreateUpdateView(APIView):
    permission_classes = (OwnerPermission, )

    @swagger_auto_schema(request_body=PitchSerializer)
    def post(self, request):
        user = request.user
        serializer = PitchSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        images = validated_data.pop('images')
        pitch = Pitch.objects.create(user=user, **validated_data)
        PitchMedia.objects.filter(id__in=images).update(pitch=pitch)
        return Response(data={
            'data': 'Football pitch created successfully!'

        })

    @swagger_auto_schema(request_body=PitchSerializer)
    def put(self, request, pk):
        user = request.user
        serializer = PitchSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        images = validated_data.pop('images')
        Pitch.objects.filter(id=pk).update(user=user, **validated_data)
        PitchMedia.objects.filter(id__in=images).update(pitch_id=pk)
        return Response(data={
            'data': 'Football pitch updated successfully!'

        })


class PitchImgUploadView(APIView):
    permission_classes = (OwnerPermission, )
    parser_classes = (MultiPartParser, FormParser)

    @swagger_auto_schema(manual_parameters=[openapi.Parameter(
        name="file",
        in_=openapi.IN_FORM,
        type=openapi.TYPE_FILE,
        required=True,
        description="Document"
    )], )
    def post(self, request):
        files = request.FILES.getlist('file')
        id_list = []
        for file in files:
            img = PitchMedia.objects.create(
                image=file,
            )
            id_list.append(img.id)
        return Response(
            id_list
        )


class PitchDetailView(APIView):
    permission_classes = (OwnerPermission|UserPermission, )

    def get(self, request, pk):
        pitch = Pitch.objects.prefetch_related('images').get(id=pk)
        serializer = PitchModelSerializer(pitch)
        return Response(data=serializer.data)


class UserPitchListView(APIView):
    permission_classes = (OwnerPermission, )

    def get(self, request):
        pitches = Pitch.objects.filter(user=request.user)
        ser = PitchModelSerializer(pitches, many=True)
        return Response(data=ser.data)


class PitchListView(APIView):
    permission_classes = (OwnerPermission|UserPermission, )

    def haversine(self, lat1, lon1, lat2, lon2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles
        return c * r

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter('activity_time', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING),
        openapi.Parameter('geolocation', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING),
    ])
    def get(self, request):
        query_params = dict(request.query_params)
        start_activity_time = query_params.get('activity_time')
        geolocation = query_params.get('geolocation')
        if start_activity_time:
            start_activity_time = start_activity_time[0]
        if geolocation:
            geolocation = geolocation[0]

        lat, lon = geolocation.split(',')

        pitch_list = []
        pitches = Pitch.objects.all().prefetch_related('pitch_orders')
        for pitch in pitches:
            falls = pitch.pitch_orders.filter(
                Q(start_activity_time__lt=start_activity_time) &
                Q(finish_activity_time__gt=start_activity_time)
            )
            if not falls:
                pitch_list.append(pitch)
                pitch_lat, pitch_lon = pitch.geolocation.split(',')
                pitch.distance = self.haversine(float(lat), float(lon), float(pitch_lat), float(pitch_lon))
        pitches = sorted(pitches, key=lambda p: p.distance)
        ser = PitchModelSerializer(pitches, many=True)
        return Response(data=ser.data)




