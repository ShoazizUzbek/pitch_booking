from django.urls import path

from football_pitch.views.order import BookPitchView, BookPitchListView, DeleteBookPitchView
from football_pitch.views.pitch_entity import PitchCreateUpdateView, PitchImgUploadView, PitchDetailView, PitchListView, \
    UserPitchListView

urlpatterns = [
    path('pitch/create-update', PitchCreateUpdateView.as_view(),),
    path('pitch/create-update/<int:pk>', PitchCreateUpdateView.as_view(),),
    path('pitch/media-upload', PitchImgUploadView.as_view(),),
    path('pitch/detail/<int:pk>', PitchDetailView.as_view(),),
    path('all/pitch/list', PitchListView.as_view(),),
    path('user/pitch/list', UserPitchListView.as_view(),),
    path('book/pitch', BookPitchView.as_view(),),
    path('book/pitch/<int:pk>/list', BookPitchListView.as_view(),),
    path('book/pitch/delete/<int:pk>', DeleteBookPitchView.as_view(),),

]
