from django.db import models

# Create your models here.
from user.models import User


class Pitch(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pitches', )
    name = models.CharField(max_length=155, )
    address = models.CharField(max_length=255, )
    geolocation = models.CharField(max_length=255, )  # lattitude,longitude
    contact = models.CharField(max_length=50, )
    cost_by_hour = models.IntegerField()


class PitchMedia(models.Model):
    pitch = models.ForeignKey(Pitch, on_delete=models.SET_NULL, null=True, related_name='images', )
    image = models.ImageField(upload_to='pitches')


class PitchOrder(models.Model):
    pitch = models.ForeignKey(Pitch, on_delete=models.CASCADE, related_name='pitch_orders', )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_orders', )
    start_activity_time = models.DateTimeField()
    finish_activity_time = models.DateTimeField()
    price = models.IntegerField()
    activity_hours = models.IntegerField()