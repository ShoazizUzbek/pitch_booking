from django.contrib import admin

# Register your models here.
from football_pitch.models import PitchOrder, Pitch

class PitchOrderInline(admin.TabularInline):
    fields = ('price', 'user', 'start_activity_time', 'finish_activity_time','activity_hours')
    extra = 0
    model = PitchOrder

class PitchAdmin(admin.ModelAdmin):
    list_display = ('name', )
    inlines = [PitchOrderInline]
admin.site.register(Pitch, PitchAdmin)
