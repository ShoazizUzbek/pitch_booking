from rest_framework import serializers

from football_pitch.models import Pitch


class PitchSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    geolocation = serializers.CharField()
    contact = serializers.CharField()
    cost_by_hour = serializers.IntegerField()
    images = serializers.ListSerializer(child=serializers.IntegerField())


class PitchMediaSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    image = serializers.ImageField()


class PitchModelSerializer(serializers.ModelSerializer):
    images = PitchMediaSerializer(many=True, read_only=True)

    class Meta:
        model = Pitch
        fields = ('id', 'name', 'images')
