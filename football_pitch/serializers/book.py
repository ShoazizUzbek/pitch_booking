from rest_framework import serializers


class PitchBookSerializer(serializers.Serializer):
    pitch_id = serializers.IntegerField()
    start_activity_time = serializers.DateTimeField()
    activity_hours = serializers.IntegerField()
