FROM python:3.8
ENV PROJECT_ROOT /app
WORKDIR $PROJECT_ROOT
COPY . $PROJECT_ROOT
RUN pip3 install --no-cache-dir -r requirements.txt
